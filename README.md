Ansible Roles
===================
This is a collection of Ansible Roles, each role has its own repository and its linked here as submodule just for ease.

To clone this repository with all the submodule at once use: `git clone --recursive`